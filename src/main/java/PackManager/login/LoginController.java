package PackManager.login;

import PackManager.user.*;
import PackManager.util.*;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;
import spark.Route;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

import static PackManager.Main.userDao;
import static PackManager.util.RequestUtil.*;
import static spark.Spark.halt;

public class LoginController {

    public static Route handleLoginPost = (Request request, Response response) -> {
        Map<String, String> responseContent = new HashMap<>();
        Map<String, String> authMap;
        try {
            authMap = getQueryCredentials(request);
        }catch  (IllegalArgumentException e){
            return errorResponse.illegalArgument(e, response);
        }
        if (!UserController.authenticate(authMap.get("username"), authMap.get("password"))) {
            response.status(HttpStatus.UNAUTHORIZED_401);
            return response;
        }
        User user = userDao.getUserByEmail(authMap.get("username"));

        request.session().attribute("currentUser", authMap.get("username"));
        request.session().attribute("currentUserID", user.getId().toString());
        response.status(HttpStatus.OK_200);

        responseContent.put("authenticated", "true");
        responseContent.put("userId", user.getId().toString());

        //response.body("{authenticated : true}");
        return responseContent;
    };

    public static Route handleLogoutPost = (Request request, Response response) -> {
        request.session().removeAttribute("currentUser");
        request.session().attribute("loggedOut", true);
        response.status(HttpStatus.OK_200);
        response.body("{authenticated : false}");
        return response;
    };

    public static void ensureUserIsLoggedIn(Request request, Response response) {
        if (request.session().attribute("currentUser") == null) {
            response.status(403);
            halt(403, "{error : 403, errorDescription: \"unauthorized!\"}");
        }
    }

    public static String getSessionUserID(Request request){
        System.out.println("Retrieving current user id: " + request.session().attribute("currentUser"));
        return request.session().attribute("currentUserID");
    }

}
