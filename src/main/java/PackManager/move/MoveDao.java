package PackManager.move;


import PackManager.box.Box;
import PackManager.util.Validate;
import com.mongodb.BasicDBList;
import com.mongodb.Cursor;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;
import org.bson.types.ObjectId;

import static PackManager.Main.userDao;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;

public class MoveDao {
    private static MongoCollection<Document> movesCollection;

    public MoveDao(final MongoDatabase packManDB) {
        movesCollection = packManDB.getCollection("moves");
    }

    static ArrayList<Move> getMovesByUserId(String userID){
        ArrayList<Move> moves = new ArrayList<Move>();
        //Get user doc with projection for just the moves array.
        //Retrieve each move by its ID and convert to arraylist.

        ArrayList moveIDs = userDao.getMovesForUser(userID);

        MongoCursor cursor = movesCollection.find(in("_id", moveIDs)).iterator();
            while(cursor.hasNext()){
                moves.add(new Move((Document) cursor.next()));
            }
        return moves;
    }

    static ObjectId createMove(Move move, String userID){
        if(!Validate.name(move.getName())){
            throw new IllegalArgumentException("Name is required for new move.");
        }
        Document newMove = new Document()
                .append("name", move.getName())
                .append("completed", move.getCompleted());

        movesCollection.insertOne(newMove);
        userDao.addMoveToUser(userID, newMove.getObjectId("_id"));

        return newMove.getObjectId("_id");
    }

    public static Move getMoveById(ObjectId id){
        return new Move(movesCollection.find(eq("_id", id)).first());
    }

    static void deleteMove (String moveId, String userID) throws AccessDeniedException, MongoWriteException{
        if(isUserAuthForMove(userID, moveId));
        userDao.removeMoveFromUser(userID, new ObjectId(moveId));

        //Delete Move from moves
        DeleteResult result = movesCollection.deleteOne(eq("_id", new ObjectId(moveId)));
    }

    public static boolean isUserAuthForMove(String userID, String moveID)throws AccessDeniedException{
        ArrayList moveIDs = userDao.getMovesForUser(userID);
        if(!moveIDs.contains(new ObjectId(moveID))){
            throw new AccessDeniedException("User not authorized to delete this Move");
        };
        return true;
    }
}
