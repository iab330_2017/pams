package PackManager.move;

import PackManager.box.Box;
import PackManager.item.Item;
import PackManager.location.Location;
import PackManager.util.Validate;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBObject;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Iterator;

public class Move {
    private String id;
    private String name;
    private Location startLocation;
    private Location endLocation;
    private Boolean completed;
    private ArrayList<Box> boxes;
    private ArrayList<Item> unBoxedItems;

    public Move(String name){
        this.name = name;
        this.completed = false;
        this.startLocation = null;
        this.endLocation = null;
    }

    public Move(Document mongoDoc){
        //Builds move based on Mongo document from database.
        //TODO: complete this function. need to add all the data.
        this.name = mongoDoc.getString("name");
        this.id = mongoDoc.getObjectId("_id").toString();

        if(mongoDoc.containsKey("startLocation")){
            this.startLocation = new Location(Document.parse(mongoDoc.getString("startLocation")));
        }else{
            this.startLocation = null;
        }

//        if(mongoDoc.containsKey("endLocation")){
//            this.endLocation = new Location((Document)mongoDoc.get("endLocation"));
//        }else{
//            this.endLocation = null;
//        }
//
//        if(mongoDoc.containsKey("completed")){
//            this.completed = mongoDoc.getBoolean("completed");
//        }
//
//        if(mongoDoc.containsKey("boxes")){
//            //TODO: Error handling?
//            this.boxes = (ArrayList)mongoDoc.get("boxes");
//        }
//
//        if(mongoDoc.containsKey("unBoxedItems")){
//            //TODO: Error handling?
//            this.unBoxedItems = (ArrayList)mongoDoc.get("unBoxedItems");
//        }

    }

    public Move (JsonObject jObj){
        if(!jObj.has("name")){
            throw new IllegalArgumentException("Missing JSON Parameter. name is required");
        }
        name = jObj.get("name").getAsString();
        if(!Validate.name(name)){
            throw new IllegalArgumentException("Invalid JSON Parameter name is required");
        }
        this.name = name;
        this.completed = false;

        //TODO: What do we do if the start location is invalid?
        //Add the valid data to the database or do nothing and return 406?
        //Check for other valid parameters.
        if(jObj.has("startLocation")){
            this.startLocation = new Location(jObj.get("startLocation").getAsJsonObject());
        }

        if(jObj.has("endLocation")){
            this.endLocation = new Location(jObj.get("endLocation").getAsJsonObject());
        }

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(Location startLocation) {
        this.startLocation = startLocation;
    }

    public Location getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(Location endLocation) {
        this.endLocation = endLocation;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public int addBox(Box box){

        return -1;
    }

    public boolean removeBox(int id){

        return false;
    }

    public int addUnboxedItem(Item item){

        return -1;
    }

    public boolean removeUnboxedItem(Item item){

        return false;
    }

    public int getNumBoxes(){
        return this.boxes.size();
    }

    public int getNumUnBoxedItems(){
        return this.unBoxedItems.size();
    }

    public Iterator<Box> getBoxes(){
        return this.boxes.iterator();
    }

    public Iterator<Item> getUnBoxedItems(){
        return this.unBoxedItems.iterator();
    }
}
