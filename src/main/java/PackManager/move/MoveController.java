package PackManager.move;


import PackManager.login.LoginController;
import PackManager.util.errorResponse;
import com.google.gson.JsonSyntaxException;
import com.mongodb.MongoWriteException;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;
import spark.Route;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import static PackManager.login.LoginController.getSessionUserID;
import static PackManager.util.JsonUtil.*;
import static PackManager.util.errorResponse.*;
import static PackManager.Main.moveDao;

public class MoveController {
    public static Route getMoves = (Request request, Response response) -> {
        Map<String, String> responseMap = new HashMap<String, String>();
        ArrayList<Move> moves;
        String userID = LoginController.getSessionUserID(request);
        System.out.println("Getting userid from session for moves retrieval: " + userID);
        try {
            moves = moveDao.getMovesByUserId(userID);
        }catch(IllegalArgumentException e){
            return e.getMessage();
            //return invalidUserID(response);
        }
        if(moves == null || moves.size() < 1){
            return itemNotFoundInDatabase(response, "No moves were found in database for the current user.");
        }
        response.status(HttpStatus.OK_200);
        return moves;
    };


    public static Route createMove = (Request request, Response response) -> {
        Map<String, String> responseMap = new HashMap<String, String>();
        Move newMove;
        try{
            newMove = new Move(toJsonObj(request.body()));
        }catch(IllegalArgumentException e){
            return illegalArgument(e, response);
        }catch(JsonSyntaxException e){
            System.out.println("Caught malformed json exception");
            return malformedJson(e, response);
        }
        String newID = "";
        try{
            newID = moveDao.createMove(newMove, getSessionUserID(request)).toString();
        }catch(MongoWriteException e){
            response.status(HttpStatus.BAD_REQUEST_400);
            return e.getError();
        }
        response.status( HttpStatus.CREATED_201);
        responseMap.put("id", newID);
        return responseMap;
    };


    public static Route updateMove = (Request request, Response response) -> {

        return response;
    };


    public static Route deleteMove = (Request request, Response response) -> {
        if(request.params().containsKey(":id")){
            try{
                moveDao.deleteMove(request.params(":id"), getSessionUserID(request));
            }catch(MongoWriteException e){
                return errorResponse.itemNotFoundInDatabase(response, "Unable to delete item. Most likely it does not exist in database.");
            }catch (AccessDeniedException e){
                return errorResponse.unauthorizedAccess(response);
            };
        }
        response.status(HttpStatus.NO_CONTENT_204);
        //response.body("{\"success\" : \"true\"}");
        return response;
    };
}
