package PackManager;

import PackManager.box.BoxController;
import PackManager.box.BoxDao;
import PackManager.item.ItemController;
import PackManager.item.ItemDao;
import PackManager.location.LocationCodecProvider;
import PackManager.location.LocationController;
import PackManager.location.LocationDao;
import PackManager.login.LoginController;
import PackManager.move.MoveController;
import PackManager.move.MoveDao;
import PackManager.room.RoomController;
import PackManager.room.RoomDao;
import PackManager.user.UserController;
import PackManager.user.UserDao;
import PackManager.util.Filters;
import PackManager.util.Path;
import com.mongodb.*;
import com.mongodb.client.MongoDatabase;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import spark.Filter;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static PackManager.util.JsonUtil.json;
import static spark.Spark.*;
import static spark.debug.DebugScreen.*;

public class Main {
    public static BoxDao boxDao;
    public static ItemDao itemDao;
    public static LocationDao locationDao;
    public static MoveDao moveDao;
    public static RoomDao roomDao;
    public static UserDao userDao;
    public static LoginController loginController;
    public static CodecRegistry codecRegistry;

    public static void main(String[] args) {

        final MongoDatabase packManDB = initDatabase();

        // Instantiate your dependencies
        boxDao      = new BoxDao(packManDB);
        itemDao     = new ItemDao(packManDB);
        locationDao = new LocationDao();
        moveDao     = new MoveDao(packManDB);
        roomDao     = new RoomDao(packManDB);
        userDao     = new UserDao(packManDB);

        // Configure Spark
        final String portNumber = System.getenv("PORT");
        //port 4567); //FOR DEV ONLY
        if (portNumber != null) {
            port(Integer.parseInt(portNumber));
        }else{
            port(4567);
        }
        //TODO: Setup keystores to enable https.
        //secure(keystoreFilePath, keystorePassword, truststoreFilePath, truststorePassword);
        enableDebugScreen();

        // Set up before-filters (called before each get/post)
        before("*",         Filters.addTrailingSlashes);
        before("*",         Filters.clientAcceptsJson);
        before("/api/*",    Filters.checkAuthorized);

        // Set up routes
        get   (Path.USER,          "application/json", UserController.getUserDetails, json());
        post  (Path.NEW_USER,      "application/json", UserController.addNewUser, json());
        put   (Path.USER,          "application/json", UserController.updateUser, json());

        get   (Path.MOVES,         "application/json", MoveController.getMoves, json());
        post  (Path.MOVES,         "application/json", MoveController.createMove, json());
        put   (Path.MOVE,          "application/json", MoveController.updateMove, json());
        delete(Path.MOVE,          "application/json", MoveController.deleteMove);

        get   (Path.ROOMS_IN_MOVE, "application/json", RoomController.getRooms, json());
        post  (Path.ROOMS_IN_MOVE, "application/json", RoomController.createRoom);
        put   (Path.ROOM_IN_MOVE,  "application/json", RoomController.updateRoom, json());
        delete(Path.ROOM_IN_MOVE,  "application/json", RoomController.deleteRoom);

        get   (Path.BOXES_BY_MOVE, "application/json", BoxController.getBoxes, json());
        post  (Path.BOXES_BY_MOVE, "application/json", BoxController.createBox, json());
        put   (Path.BOX_BY_MOVE,   "application/json", BoxController.updateBox);
        delete(Path.BOX_BY_MOVE,   "application/json", BoxController.deleteBox);

        get   (Path.ITEMS_IN_BOX,  "application/json", ItemController.getItems, json());
        post  (Path.ITEMS_IN_BOX,  "application/json", ItemController.createItem, json());
        put   (Path.ITEM_IN_BOX,   "application/json", ItemController.updateItem);
        delete(Path.ITEM_IN_BOX,   "application/json", ItemController.deleteItem);

//        get (Path.LOCATIONS,      "application/json", LocationController.getLocations, json());
//        post(Path.LOCATIONS,      "application/json", LocationController.createLocation, json());
//        put (Path.LOCATION,       "application/json", LocationController.updateLocation, json());
//        delete(Path.LOCATION,     "application/json", LocationController.deleteLocation);

        post(Path.LOGIN,         "application/json", LoginController.handleLoginPost, json());
        post(Path.LOGOUT,        "application/json", LoginController.handleLogoutPost);
        get("*",            ((request, response) -> {
            response.status(404);
            return "{\"error\" : \"404\"}";
        }));

        //Set up after-filters (called after each get/post)
        after("*", Filters.addGzipHeader);
        after("*", Filters.addJsonHeader);
    }

    private static MongoDatabase initDatabase(){

        //Setup custom codec registry
        codecRegistry = CodecRegistries.fromRegistries(
                CodecRegistries.fromProviders(new LocationCodecProvider()),
                MongoClient.getDefaultCodecRegistry());

        //Setup  connection options and attache codec registry
        MongoClientOptions options = MongoClientOptions.builder()
                .codecRegistry(codecRegistry)
                .sslEnabled(true)
                .requiredReplicaSetName("PackManagerCluster-shard-0")
                .build();

        //Setup credentials.
        char[] password = "6m0QnaxEC^U16Ua".toCharArray();
        MongoCredential credential = MongoCredential.createCredential("pams", "admin", password);

        //Setup shard array.
        List serverArray =  Arrays.asList(
                new ServerAddress("packmanagercluster-shard-00-00-6h4z1.mongodb.net", 27017),
                new ServerAddress("packmanagercluster-shard-00-01-6h4z1.mongodb.net", 27017),
                new ServerAddress("packmanagercluster-shard-00-02-6h4z1.mongodb.net", 27017));

        final MongoClient mongoClient = new MongoClient(serverArray, Arrays.asList(credential),  options);

        return mongoClient.getDatabase("packMan");
    }
}