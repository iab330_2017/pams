package PackManager.box;

import PackManager.login.LoginController;
import PackManager.util.errorResponse;
import com.google.gson.JsonSyntaxException;
import com.mongodb.MongoWriteException;
import org.bson.types.ObjectId;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;
import spark.Route;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static PackManager.Main.boxDao;
import static PackManager.login.LoginController.getSessionUserID;
import static PackManager.util.JsonUtil.toJsonObj;
import static PackManager.util.errorResponse.illegalArgument;
import static PackManager.util.errorResponse.itemNotFoundInDatabase;
import static PackManager.util.errorResponse.malformedJson;

public class BoxController {

    public static Route getBoxes = (Request request, Response response) -> {
        ArrayList<Box> boxes;
        String userID = getSessionUserID(request);
        String moveID;

        if(!request.params().containsKey(":moveid")){
            return errorResponse.illegalArgument(
                    new IllegalArgumentException("Move ID missing from URL. Follow the format: /api/move/:moveId/box/"),
                    response);
        }

        moveID = request.params().get(":moveid");

        //get move from parameters.
        System.out.println("Getting userid from session for boxes retrieval: " + userID);
        try {
            boxes = boxDao.getBoxesByMoveId(moveID, userID);
        }catch(IllegalArgumentException e){
            return e.getMessage();
            //return invalidUserID(response);
        }
        if(boxes == null || boxes.size() < 1){
            return itemNotFoundInDatabase(response, "No boxes were found in database for the move.");
        }
        System.out.println("Found: " + boxes.size() + " boxes. Returning to requester");
        response.status(HttpStatus.OK_200);
        return boxes;
    };

    public static Route createBox = (Request request, Response response) -> {
        Map<String, String> responseMap = new HashMap<String, String>();
        Box newBox;
        if(!request.params().containsKey(":moveid")){
            errorResponse.illegalArgument(new IllegalArgumentException("move ID missing from request URl or URL malformed."), response);
        }
        try{
            newBox = new Box(toJsonObj(request.body()), request.params(":moveid"));
        }catch(IllegalArgumentException e){
            return illegalArgument(e, response);
        }catch(JsonSyntaxException e){
            System.out.println("Caught malformed json exception");
            return malformedJson(e, response);
        }
        String newID = "";
        try{
            newID = boxDao.createBox(newBox, request.params("moveId")).toString();
        }catch(MongoWriteException e){
            response.status(HttpStatus.BAD_REQUEST_400);
            return e.getError();
        }
        response.status( HttpStatus.CREATED_201);
        responseMap.put("id", newID);
        return responseMap;
    };

    public static Route deleteBox = (Request request, Response response) -> {
        if(request.params().containsKey(":moveid") && request.params().containsKey(":boxid")){
            try{
                boxDao.deleteBox(request.params(":moveid"), request.params(":boxid"), getSessionUserID(request));
            }catch(MongoWriteException e){
                return errorResponse.itemNotFoundInDatabase(response, "Unable to delete item. Most likely it does not exist in database.");
            }catch (AccessDeniedException e){
                return errorResponse.unauthorizedAccess(response);
            };
        }
        response.status(HttpStatus.OK_200);
        response.body("{\"success\" : \"true\"}");
        return response;
    };

    public static Route updateBox = (Request request, Response response) -> {

        if(!request.params().containsKey(":moveid") &&
                !request.params().containsKey(":boxid" )){
            return errorResponse.illegalArgument(
                    new IllegalArgumentException("URL missing parameters. Follow the format /api/move/:moveId/box/:boxId/"),
                    response);
        }
        String moveId = request.params(":moveid");
        String boxId  = request.params(":boxid");

        //TODO:// ADD MORE EXCEPTION HANDLING
        boxDao.updateBoxByMoveId(toJsonObj(request.body()),boxId, moveId, getSessionUserID(request));
        return response;
    };
}
