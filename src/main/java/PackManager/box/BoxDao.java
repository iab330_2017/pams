package PackManager.box;

import PackManager.move.MoveDao;
import PackManager.room.Room;
import PackManager.util.JsonUtil;
import PackManager.util.Validate;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.mongodb.Cursor;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.print.Doc;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;

import static PackManager.Main.moveDao;
import static PackManager.Main.userDao;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.pull;
import static com.mongodb.client.model.Updates.push;

public class BoxDao {
    private static MongoCollection<Document> movesCollection;
    private static MongoCollection<Document> boxesCollection;

    public BoxDao(final MongoDatabase packManDB) {
        movesCollection = packManDB.getCollection("moves");
        boxesCollection = packManDB.getCollection("boxes");
    }

    ArrayList<Box> getBoxesByMoveId(String moveID, String userID) throws AccessDeniedException{
        ArrayList<Box> boxes = new ArrayList<Box>();
        ArrayList<Document> results;
        if(moveDao.isUserAuthForMove(userID, moveID)){
            MongoCursor cursor = boxesCollection.find(eq("moveId", new ObjectId(moveID))).iterator();

            while (cursor.hasNext()){
                boxes.add(new Box((Document)cursor.next()));
            }
        }
        return boxes;
    }

    public ObjectId createBox(Box box, String moveId) {
        String json = JsonUtil.toJson(box);

       /* "_id"      : "mongo unique ObjectID",
                "packed"   : false,
                "sent"     : false,
                "fragile"  : true,
                "priority" : "medium",
                "destRoom" : "_id"*/

        Document newBox = new Document()
                .append("_id"     , new ObjectId())
                .append("moveId"  , new ObjectId(box.getMoveId()))
                .append("name"    , box.getName())
                .append("packed"  , box.getPacked())
                .append("sent"    , box.getSent())
                .append("fragile" , box.getFragile())
                //.append("priority", box.priority.get());
                .append("priority", box.getPriority())
                .append("destinationRoomId", box.getDestinationRoom());

        boxesCollection.insertOne(newBox);
        return newBox.getObjectId("_id");
    }

    public void deleteBox(String moveId, String boxId, String userId) throws AccessDeniedException{
        if(MoveDao.isUserAuthForMove(userId, moveId)){
            //Delete Box from move
            DeleteResult result = boxesCollection.deleteOne(eq("_id", new ObjectId(boxId)));
//            UpdateResult result =  movesCollection.updateOne(
//                    eq("_id", new ObjectId(moveId)),
//                    pull("boxes", eq("_id", new ObjectId(boxId))));

            System.out.println("Attempted to delete box: " + boxId + " from the database. " +
                    " Deleted: " + result.getDeletedCount()
            );
        };
    }

    public void updateBoxByMoveId(JsonObject json, String boxId, String moveId, String userId) throws AccessDeniedException{
        if(MoveDao.isUserAuthForMove(userId, moveId)){

            Document filter = new Document()
                    .append("_id", new ObjectId(boxId));

            ArrayList update = Box.buildUpdateQuery(json, "");

            UpdateResult result = boxesCollection.updateOne(
                    filter,
                    combine(update)
            );
            System.out.println("Attempted to update box: " + boxId + " in the database. " +
                    " Matched: " + result.getMatchedCount() +
                    " Modified: " + result.getModifiedCount()
            );
        };

    }

}
