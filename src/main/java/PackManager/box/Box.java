package PackManager.box;

import PackManager.item.Item;
import PackManager.room.Room;
import PackManager.util.Priority;
import PackManager.util.Validate;
import com.google.gson.JsonObject;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Iterator;

public class Box {

    /*		"_id"      : "mongo unique ObjectID",
			"packed"   : false,
			"sent"     : false,
			"fragile"  : true,
			"priority" : "medium",
			"destRoom" : "_id",*/
    private String id;
    private String moveId;
    private boolean packed;
    private boolean sent;
    private boolean fragile;
    private int priority;
    //public  Priority priority;
    private String destinationRoomId;
    private ArrayList<Item> items;

    private String name;

    public Box(ObjectId ID){
        this.id = ID.toString();
    }

    public Box(Document mongoDoc){

        if(mongoDoc.containsKey("_id")){
            this.id = mongoDoc.getObjectId("_id").toString();
        }else{
            this.id = null;
        }

        if(mongoDoc.containsKey("packed")){
            this.packed = mongoDoc.getBoolean("packed");
        }else{
            this.packed = false;
        }

        if(mongoDoc.containsKey("set")){
            this.sent = mongoDoc.getBoolean("set");
        }else{
            this.sent = false;
        }

        if(mongoDoc.containsKey("sent")){
            this.sent = mongoDoc.getBoolean("sent");
        }else{
            this.sent = false;
        }

        if(mongoDoc.containsKey("fragile")){
            this.fragile = mongoDoc.getBoolean("fragile");
        }else{
            this.fragile = false;
        }

        this.priority = 0;//new Priority();
        if(mongoDoc.containsKey("priority")){
            //this.priority.setPriority(mongoDoc.getInteger("priority"));
            this.priority = mongoDoc.getInteger("priority");
        }//else{
//            //this.priority.notSet();
//        }

        if(mongoDoc.containsKey("name")){
            this.name = mongoDoc.getString("name");
        }else{
            this.name = "";
        }

        if(mongoDoc.containsKey("moveId")){
            this.moveId = mongoDoc.getObjectId("moveId").toString();
        }else {
            this.moveId = "";
        }

        if(mongoDoc.containsKey("destinationRoomId")){
            //TODO: implement Room codec to allow the bellow to operate as expected.
            this.destinationRoomId = mongoDoc.getString("destinationRoomId");
        }

    }

    public Box(JsonObject jObj){

        if(jObj.has("packed")){
            this.packed = jObj.get("packed").getAsBoolean();
        }else{
            this.packed = false;
        }

        if(jObj.has("set")){
            this.sent = jObj.get("set").getAsBoolean();
        }else{
            this.sent = false;
        }

        if(jObj.has("sent")){
            this.sent = jObj.get("sent").getAsBoolean();
        }else{
            this.sent = false;
        }

        if(jObj.has("fragile")){
            this.fragile = jObj.get("fragile").getAsBoolean();
        }else{
            this.fragile = false;
        }

        this.priority = 0; //new Priority();
        if(jObj.has("priority")){
            //this.priority.setPriority(jObj.get("priority"));
            //this.priority.notSet();
            this.priority = jObj.get("priority").getAsInt();
        }/*else{
            this.priority.notSet();
        }*/

        if(jObj.has("name")){
            this.name = jObj.get("name").getAsString();
        }else{
            this.name = "";
        }

        if(jObj.has("moveId")){
            this.moveId = jObj.get("moveId").getAsString();
        }else {
            this.moveId = "";
        }


        if(jObj.has("destinationRoomId")){
            //TODO: implement Room codec to allow the bellow to operate as expected.
            this.destinationRoomId = jObj.get("destinationRoomId").getAsString();
        }

    }

    public Box(JsonObject jObj, String moveId){
        this(jObj);
        this.moveId = moveId;
    }

    public boolean markSent(){
        this.sent = true;
        return this.sent;
    }

    public boolean markNotSent(){
        this.sent = false;
        return this.sent;
    }

    public boolean getSent(){
        return this.sent;
    }

    public boolean markPacked(){
        this.packed = true;
        return this.packed;
    }

    public boolean markNotPacked(){
        this.packed = false;
        return this.packed;
    }

    public boolean getPacked(){
        return this.packed;
    }

    public boolean markFragile(){
        this.fragile = true;
        return this.fragile;
    }

    public boolean markNotFragile(){
        this.fragile = false;
        return this.fragile;
    }

    public String getName(){
        return name;
    }

    public void  setName(String name){
        this.name = name;
    }

    public boolean getFragile(){
        return this.fragile;
    }

    public String setDestinationRoom(String roomId){
        return this.destinationRoomId = roomId;
    }

    public String getDestinationRoom(){
        return this.destinationRoomId;
    }

    public void removeDestinationRoom(){
        this.destinationRoomId = null;
    }

    public boolean addItem(Item item){
        return this.items.add(item);
    }

    public Iterator<Item> getItemsIterator(){
        return this.items.iterator();
    }

    public String getMoveId(){
        return this.moveId;
    }

    public void setMoveId(String moveId){
        this.moveId = moveId;
    }

    public void setMoveId(ObjectId moveId){
        this.moveId = moveId.toString();
    }

    public String getId() {
        return id;
    }

    public int getPriority(){
        return this.priority;
    }

    public void setPriority(int priority){
        this.priority = priority;
    }


    public static ArrayList buildUpdateQuery(JsonObject jObj, String path) throws IllegalArgumentException{
        ArrayList updateDoc = new ArrayList();

        if(jObj.has("name")){
            if(!Validate.name(jObj.get("name").getAsString())) throw new IllegalArgumentException("Unable to update. Invalid name min 2 chars.");
            addSetRequest(updateDoc,path + "name", jObj.get("name").getAsString());
        }

        if(jObj.has("fragile")){
            addSetRequest(updateDoc,path + "fragile", jObj.get("fragile").getAsBoolean());
        }

        if(jObj.has("destinationRoomId")){
            addSetRequest(updateDoc,path + "destinationRoomId", jObj.get("destinationRoomId").getAsString());
        }

        if(jObj.has("packed")){
            addSetRequest(updateDoc,path + "packed", jObj.get("packed").getAsBoolean());
        }

        if(jObj.has("sent")){
            addSetRequest(updateDoc,path + "packed", jObj.get("packed").getAsBoolean());
        }

        if(jObj.has("priority")){
            addSetRequest(updateDoc,path + "priority", jObj.get("priority").getAsInt());
        }
        return updateDoc;
    }

    private static void addSetRequest(ArrayList doc, String key, Object obj){
        doc.add(new Document("$set", new Document().append(key, obj)));
    }
}

