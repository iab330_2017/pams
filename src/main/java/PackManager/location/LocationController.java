package PackManager.location;

import PackManager.move.Move;
import PackManager.util.errorResponse;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.HashMap;
import java.util.Map;

import static PackManager.util.JsonUtil.toJsonObj;
import static PackManager.util.errorResponse.illegalArgument;
import static PackManager.util.errorResponse.invalidUserID;
import static PackManager.util.errorResponse.malformedJson;
import static PackManager.Main.locationDao;

public class LocationController {

    public static Route createLocation = (Request request, Response response) -> {
        Map<String, String> responseMap = new HashMap<String, String>();
        Location newLocation;
        Move move = new Move(request.params("moveId").toString());
        try{
            JsonObject json = toJsonObj(request.body());

            if(json.has("startLocation")){
                move.setStartLocation( new Location(json.getAsJsonObject("startLocation")));

            }else if(json.has("endLocation")){
                move.setEndLocation( new Location(json.getAsJsonObject("endLocation")));
            }else if(json.has("alternateLocation")){
                //alternate locations have not yet been implmented.
                System.out.println("Attempted to set alternate location. This is not yet implemented.");
            }else{
                //return error as no location type is specified.
                response.status(HttpStatus.BAD_REQUEST_400);
                return response;
            }

            locationDao.addLocation(move);
        }catch(IllegalArgumentException e){
            return illegalArgument(e, response);
        }catch(JsonSyntaxException e){
            System.out.println("Caught malformed json exception");
            return malformedJson(e, response);
        }

        return response;
    };

    public static Route getLocations = (Request request, Response response) -> {

        return response;
    };

    public static Route updateLocation = (Request request, Response response) -> {

        return response;
    };

    public static Route deleteLocation = (Request request, Response response) -> {

        return response;
    };
}
