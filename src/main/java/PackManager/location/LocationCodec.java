package PackManager.location;

import org.bson.BsonReader;
import org.bson.BsonType;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;

import java.util.ArrayList;
import java.util.List;

public class LocationCodec implements Codec<Location> {
    private CodecRegistry codecRegistry;

    public LocationCodec(CodecRegistry codecRegistry) {
        this.codecRegistry = codecRegistry;
    }
    @Override
    public Location decode(BsonReader reader, DecoderContext decoderContext) {
        reader.readStartDocument();

        //String id          = reader.readString("_id");
        String name        = reader.readString("name");
        String street      = reader.readString("street");
        String suburb      = reader.readString("suburb");
        String postcode    = reader.readString("postcode");
        String state       = reader.readString("state");
        String description = reader.readString("description");

        /*Codec<Document> roomCodec = codecRegistry.get(Document.class);
        ArrayList<Document> rooms = new ArrayList<>();
        reader.readStartArray();
        while (reader.readBsonType() != BsonType.END_OF_DOCUMENT) {
            rooms.add(roomCodec.decode(reader, decoderContext));
        }
        reader.readEndArray();*/
        //reader.readEndDocument();

        //Location location = new Location(id);
        Location location = new Location(name);
        location.setStreet(street);
        location.setSuburb(suburb);
        location.setPostcode(postcode);
        location.setState(state);
        location.setDescription(description);
        //location.setRooms(rooms);

        return location;
    }

    @Override
    public void encode(BsonWriter writer, Location location, EncoderContext encoderContext) {
        writer.writeStartDocument();

        writer.writeName("name");
        writer.writeString(location.getName());

        writer.writeName("street");
        writer.writeString(location.getStreet());

        writer.writeName("suburb");
        writer.writeString(location.getSuburb());

        writer.writeName("postcode");
        writer.writeString(location.getPostcode());

        writer.writeName("state");
        writer.writeString(location.getState());

        writer.writeName("description");
        writer.writeString(location.getDescription());

//        writer.writeStartArray("rooms");
//        for (Document document : location.getRooms()) {
//            Codec<Document> documentCodec = codecRegistry.get(Document.class);
//            encoderContext.encodeWithChildContext(documentCodec, writer, document);
//        }
//        writer.writeEndArray();
        writer.writeEndDocument();
    }

    @Override
    public Class<Location> getEncoderClass() {
        return Location.class;
    }
}
