package PackManager.location;

import PackManager.room.Room;
import com.google.gson.JsonObject;
import org.bson.Document;

import java.util.ArrayList;

public class Location {
    /*		"name" 		  : "Old House",
		"street" 	  : "Rode Road",
		"suburb" 	  : "Burb",
		"postcode"    : "a1234",
		"state" 	  : "QLD",
		"description" : "Our old house.",
		"rooms"	      : [
			{"name" : "Living Room", "icon" : "_id"},
			{"name" : "Kitchen", "icon" : "_id"},
			{"name" : "Bedroom", "icon" : "_id"},
		]*/
    private String id;
    private String name;
    private String street;
    private String suburb;
    private String postcode;
    private String state;
    private String description;
    private ArrayList<Room> rooms;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<Room> getRooms() {
        return rooms;
    }

    public void setRooms(ArrayList<Room> rooms) {
        this.rooms = rooms;
    }

    public Location(JsonObject jObj){
        /*	"startLocation" : {
		"name" 		  : "Old House",
		"street" 	  : "Rode Road",
		"suburb" 	  : "Burb",
		"postcode"    : "a1234",
		"state" 	  : "QLD",
		"description" : "Our old house.",
		"rooms"	      : [
			{"name" : "Living Room", "icon" : "_id"},
			{"name" : "Kitchen", "icon" : "_id"},
			{"name" : "Bedroom", "icon" : "_id"},
		]
	},*/
        if(!jObj.has("name")){
            throw new IllegalArgumentException("Location name is required");
        }
        setName(jObj.get("name").getAsString());

        if(jObj.has("street")){
            setStreet(jObj.get("street").getAsString());
        }

        if(jObj.has("suburb")){
            setSuburb(jObj.get("suburb").getAsString());
        }

        if(jObj.has("postcode")){
            setPostcode(jObj.get("postcode").getAsString());
        }

        if(jObj.has("state")){
            setState(jObj.get("state").getAsString());
        }

        if(jObj.has("description")){
            setDescription(jObj.get("description").getAsString());
        }

        if(jObj.has("rooms")){
            //setRooms((ArrayList<Room>) jObj.get("rooms").getAsJsonArray());
            System.out.println("NOT adding rooms to location object. not yet implemented.");
        }

    }

    public Location(Document mongoDoc){

    }

    public Location(String name){
        this.name =  name;
    }
}
