package PackManager.room;

import PackManager.login.LoginController;
import PackManager.move.Move;
import PackManager.util.errorResponse;
import com.google.gson.JsonSyntaxException;
import com.mongodb.MongoWriteException;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;
import spark.Route;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static PackManager.Main.roomDao;
import static PackManager.login.LoginController.getSessionUserID;
import static PackManager.util.JsonUtil.toJsonObj;
import static PackManager.util.errorResponse.*;

public class RoomController {
    public static Route getRooms = (Request request, Response response) -> {
        //Map<String, String> responseMap = new HashMap<String, String>();
        ArrayList<Room> rooms;
        try {
            rooms = roomDao.getRoomsByMoveId(request.params("moveId"));
        } catch (IllegalArgumentException e) {
            return e.getMessage();
            //return invalidUserID(response);
        }
        if (rooms == null || rooms.size() < 1) {
            return itemNotFoundInDatabase(response, "No rooms were found in database for the move.");
        }
        response.status(HttpStatus.OK_200);
        return rooms;
    };

    public static Route createRoom = (Request request, Response response) -> {
        //Map<String, String> responseMap = new HashMap<String, String>();
        Room newRoom;
        try{
            newRoom = new Room(toJsonObj(request.body()));
        }catch(IllegalArgumentException e){
            return illegalArgument(e, response);
        }catch(JsonSyntaxException e){
            System.out.println("Caught malformed json exception");
            return malformedJson(e, response);
        }
        String newID = "";
        try{
            roomDao.createRoom(newRoom, request.params("moveId"));
        }catch(MongoWriteException e){
            response.status(HttpStatus.BAD_REQUEST_400);
            return e.getError();
        }
        response.status( HttpStatus.CREATED_201);
        return response;
    };

    public static Route deleteRoom = (Request request, Response response) -> {
        //"/api/move/:moveId/room/:id"
        if(request.params().containsKey(":moveid") && request.params().containsKey(":id")){
            try{
                roomDao.deleteRoom(
                        getSessionUserID(request),
                        request.params(":moveid"),
                        request.params(":id"));
            }catch(MongoWriteException e){
                return errorResponse.itemNotFoundInDatabase(response, "Unable to delete item. Most likely it does not exist in database.");
            }catch (AccessDeniedException e){
                return errorResponse.unauthorizedAccess(response);
            };
        }else{
            return errorResponse.illegalArgument(
                    new IllegalArgumentException("URL missing parameters. Follow the format /api/move/:moveId/room/:id"),
                    response);
        }
        response.status(HttpStatus.OK_200);
        response.body("{\"success\" : \"true\"}");
        return response;
    };

    public static Route updateRoom = (Request request, Response response) -> {
        return response;
    };
}
