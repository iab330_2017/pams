package PackManager.room;

import PackManager.util.Validate;
import com.google.gson.JsonObject;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;

import static PackManager.Main.moveDao;
import static PackManager.Main.userDao;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Updates.pull;
import static com.mongodb.client.model.Updates.push;

public class RoomDao {
    private static MongoCollection<Document> movesCollection;

    public RoomDao(final MongoDatabase packManDB) {
        movesCollection = packManDB.getCollection("moves");
    }

    ArrayList<Room> getRoomsByMoveId(String moveID){
        ArrayList<Room> rooms = new ArrayList<>();
        ArrayList<Document> results;
        Document moveDoc;

        moveDoc = movesCollection.find(eq("_id", new ObjectId(moveID)))
                .projection(fields(include("rooms"), excludeId()))
                .first();

        if(moveDoc == null || !moveDoc.containsKey("rooms"))return new ArrayList();
        results = (ArrayList) moveDoc.get("rooms");

        for (Document doc : results) {
            rooms.add(new Room(doc));
        }
        return rooms;
    }

    void createRoom(Room room, String moveId) {
        if (!Validate.name(room.getName())) {
            throw new IllegalArgumentException("Name is required to add new room.");
        }
        Document newRoom = new Document()
                .append("name", room.getName());
        if(room.getId() != null && room.getId().length() > 0){
            newRoom.append("_id", room.getId());
        }else{
            newRoom.append("_id", new ObjectId());
        }

        movesCollection.findOneAndUpdate(eq("_id", new ObjectId(moveId)), push("rooms", newRoom));
    }

    void deleteRoom(String userID, String moveID, String roomID) throws AccessDeniedException{
        if(moveDao.isUserAuthForMove(userID, moveID)){
            //Document document = new Document().append("name", roomID);
            UpdateResult result =  movesCollection.updateOne(
                    eq("_id", new ObjectId(moveID)),
                    pull("rooms", eq("name", roomID)));


            System.out.println("Attempted to delete room: " + roomID + " from the database. " +
                    " Matched: " + result.getMatchedCount() +
                    " Modified: " + result.getModifiedCount()
            );
        }
    }
}