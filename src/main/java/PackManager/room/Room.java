package PackManager.room;

import PackManager.util.Validate;
import com.google.gson.JsonObject;
import org.bson.Document;
import org.bson.types.ObjectId;

public class Room {

    private String name;
    private String id = "";

    public Room(JsonObject jObj){
        if(!jObj.has("name")){
            throw new IllegalArgumentException("Missing JSON Parameter. name is required");
        }
        name = jObj.get("name").getAsString();
        if(!Validate.name(name)){
            throw new IllegalArgumentException("Invalid JSON Parameter name is required");
        }
        this.name = name;
        if(jObj.has("id")){
            id =  jObj.get("id").getAsString();
        }
    }

    public Room(Document doc){
        if(!doc.containsKey("name")){
            throw new IllegalArgumentException("Missing JSON Parameter. name is required");
        }
        name = doc.getString("name");
        if(!Validate.name(name)){
            throw new IllegalArgumentException("Invalid JSON Parameter name is required");
        }
        this.name = name;
        if(doc.containsKey("_id")){
            id =  doc.getObjectId(("_id")).toString();
        }
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public String getId(){
        return this.id;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setId(ObjectId id){
        this.id = id.toString();
    }
}
