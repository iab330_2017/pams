package PackManager.item;

import PackManager.move.MoveDao;
import PackManager.util.Validate;
import com.google.gson.JsonObject;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.print.Doc;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.Arrays;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.pull;
import static com.mongodb.client.model.Updates.push;

public class ItemDao {
    private static MongoCollection<Document> movesCollection;
    private static MongoCollection<Document> boxesCollection;

    public ItemDao(final MongoDatabase packManDB) {
        movesCollection = packManDB.getCollection("moves");
        boxesCollection = packManDB.getCollection("boxes");
    }

    static String createItem(Item item, String moveId, String boxId, String userId) throws AccessDeniedException, IllegalArgumentException{
        MoveDao.isUserAuthForMove(userId, moveId);

        if (!Validate.name(item.getName())) {
            throw new IllegalArgumentException("Name is required to add new item.");
        }
        Document newItem = new Document()
                .append("itemId", new ObjectId())
                .append("name", item.getName())
                .append("fragile", item.getFragility())
                .append("destRoom", item.getDestinationRoomId())
                .append("packed", item.getPackedState())
                .append("description", item.getDescription())
                //.append("priority", item.priority.get());
                .append("priority", item.getPriority());

        boxesCollection.findOneAndUpdate(
                eq("_id", new ObjectId(boxId)),
                push("contents", newItem));

        return newItem.getObjectId("itemId").toString();
    }

    static ArrayList<Item> getItemsByBoxId(String moveId, String boxId, String userId) throws AccessDeniedException{
        MoveDao.isUserAuthForMove(userId, moveId);
        ArrayList items = new ArrayList<Item>();

        ArrayList<Document> results;
        Document boxDoc;


        boxDoc = boxesCollection.find
                (eq("_id", new ObjectId(boxId)))
                .projection(fields(include("contents"), excludeId()))
                .first();

        if(boxDoc == null || !boxDoc.containsKey("contents"))return new ArrayList();
        results = (ArrayList) boxDoc.get("contents");

        for (Document doc : results) {
            items.add(new Item(doc));
        }

        return items;
    }

    static void deleteItemByBoxId(String itemId, String moveId, String boxId, String userId) throws AccessDeniedException{
        if(MoveDao.isUserAuthForMove(userId, moveId)){

            Document filter = new Document().append("itemId", new ObjectId(itemId));

            UpdateResult result = boxesCollection.updateOne(
                    eq("_id", new ObjectId(boxId)),
                    pull("contents", filter)
            );
            System.out.println("Attempted to delete item: " + itemId + "  from box: " + boxId + " from the database. " +
                    " Matched: " + result.getMatchedCount() +
                    " Modified: " + result.getModifiedCount()
            );
        };
    }

    static void updateItemByBoxId(JsonObject json, String itemId, String moveId, String boxId, String userId) throws AccessDeniedException{
        if(MoveDao.isUserAuthForMove(userId, moveId)){

            Document filter = new Document()
                    .append("_id", new ObjectId(boxId))
                    .append("contents.itemId", new ObjectId(itemId));

            ArrayList update = Item.buildUpdateQuery(json, "contents.$.");

            UpdateResult result = boxesCollection.updateOne(
                    filter,
                    combine(update)
            );
            System.out.println("Attempted to update item: " + itemId + "  from box: " + boxId + " from the database. " +
                    " Matched: " + result.getMatchedCount() +
                    " Modified: " + result.getModifiedCount()
            );
        };

    }
}
