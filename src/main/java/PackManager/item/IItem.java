package PackManager.item;

import org.bson.types.ObjectId;

public interface IItem {

    public ObjectId getID();
    public void setID(ObjectId id);
    public void setID(String id);

    public String getName();
    public void setName(String name);

    public void setFragility(boolean fragile);
    public boolean getFragility();

    public void setDescription(String description);
    public String getDescription();


}
