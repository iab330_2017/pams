package PackManager.item;

import PackManager.util.errorResponse;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.mongodb.MongoWriteException;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.Response;
import spark.Route;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static PackManager.Main.itemDao;
import static PackManager.login.LoginController.getSessionUserID;
import static PackManager.util.JsonUtil.toJsonObj;
import static PackManager.util.errorResponse.illegalArgument;
import static PackManager.util.errorResponse.itemNotFoundInDatabase;
import static PackManager.util.errorResponse.malformedJson;

public class ItemController {

    public static Route getItems = (Request request, Response response) -> {
        ArrayList<Item> items;
        String userId = getSessionUserID(request);

        if(!request.params().containsKey(":moveid") ||
           !request.params().containsKey(":boxid")){
            return errorResponse.illegalArgument(
                    new IllegalArgumentException("Move Id or Box Id missing from URL. Follow the format: /api/move/:moveId/box/:boxId/item/"),
                    response);
        }

        String moveId = request.params().get(":moveid");
        String boxId  = request.params().get(":boxid");

        //get move from parameters.
        System.out.println("Getting userid from session for items retrieval: " + userId);
        try {
            items = itemDao.getItemsByBoxId(moveId, boxId, userId);
        }catch(IllegalArgumentException e){
            return e.getMessage();
            //return invalidUserID(response);
        }
        if(items == null || items.size() < 1){
            return itemNotFoundInDatabase(response, "No items were found in database for the box.");
        }
        System.out.println("Found: " + items.size() + " items. Returning to requester");
        response.status(HttpStatus.OK_200);
        return items;
    };

    public static Route createItem = (Request request, Response response) -> {
        Map<String, String> responseMap = new HashMap<String, String>();
        Item newItem;
        if(!request.params().containsKey(":moveid") &&
           !request.params().containsKey(":boxid")){
            return errorResponse.illegalArgument(
                    new IllegalArgumentException("URL missing parameters. Follow the format /api/move/:moveId/box/:boxId/item/"),
                    response);
        }
        String moveId = request.params(":moveid");
        String boxId  = request.params(":boxid");

        try{
            newItem = new Item(toJsonObj(request.body()));
        }catch(IllegalArgumentException e){
            return illegalArgument(e, response);
        }catch(JsonSyntaxException e){
            System.out.println("Caught malformed json exception");
            return malformedJson(e, response);
        }
        String newID = "";
        try{
            newID = itemDao.createItem(newItem, moveId, boxId, getSessionUserID(request));
        }catch(MongoWriteException e){
            response.status(HttpStatus.BAD_REQUEST_400);
            return e.getError();
        }
        response.status( HttpStatus.CREATED_201);
        responseMap.put("id", newID);
        return responseMap;
    };

    public static Route deleteItem = (Request request, Response response) -> {
        if(!request.params().containsKey(":moveid") &&
           !request.params().containsKey(":boxid" ) &&
           !request.params().containsKey(":itemid")){
            return errorResponse.illegalArgument(
                    new IllegalArgumentException("URL missing parameters. Follow the format /api/move/:moveId/box/:boxId/item/:itemId"),
                    response);
        }
        String moveId = request.params(":moveid");
        String boxId  = request.params(":boxid");
        String itemId = request.params(":itemid");

        itemDao.deleteItemByBoxId( itemId, moveId, boxId, getSessionUserID(request));
        return response;
    };

    public static Route updateItem = (Request request, Response response) -> {
        if(!request.params().containsKey(":moveid") &&
           !request.params().containsKey(":boxid" ) &&
           !request.params().containsKey(":itemid")){
            return errorResponse.illegalArgument(
                    new IllegalArgumentException("URL missing parameters. Follow the format /api/move/:moveId/box/:boxId/item/:itemId"),
                    response);
        }
        String moveId = request.params(":moveid");
        String boxId  = request.params(":boxid");
        String itemId = request.params(":itemid");

        //TODO:// ADD MORE EXCEPTION HANDLING
        itemDao.updateItemByBoxId(toJsonObj(request.body()),itemId, moveId, boxId, getSessionUserID(request));
        return response;
    };
}
