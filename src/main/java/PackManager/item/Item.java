package PackManager.item;

import PackManager.util.Priority;
import PackManager.util.Validate;
import com.google.gson.JsonObject;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.print.Doc;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Item implements IItem {
    private String id;
    private String name;
    private String destinationRoomId;
    private String description;
    private boolean packed;
    private boolean fragile;
    private int priority;
    //Priority priority;
    //private something picture;

    Item(JsonObject jObj){
        if(!jObj.has("name") ||
           !Validate.name(jObj.get("name").getAsString())){
            throw new IllegalArgumentException("Missing JSON Parameter. name is required");
        }
        name = jObj.get("name").getAsString();

        if(jObj.has("id")){
            id =  jObj.get("id").getAsString();
        }

        if(jObj.has("fragile")){
            fragile = jObj.get("fragile").getAsBoolean();
        }

        if(jObj.has("destRoom")){
            try{
                JsonObject room = jObj.get("destRoom").getAsJsonObject();
                destinationRoomId = room.get("id").getAsString();
            }catch(Exception e){
                System.out.println("Failed to parse destRoom when building new item from json. Exception: " + e.getMessage());
                //throw new IllegalArgumentException("JSON Parameter destRoom is invalid. Failed to parse json.");
            }
        }

        if(jObj.has("description")){
            description = jObj.get("description").getAsString();
        }

        if(jObj.has("packed")){
            packed = jObj.get("packed").getAsBoolean();
        }

        priority = 0; //new Priority();
        if(jObj.has("priority")){
            //priority.setPriority(jObj.get("priority").getAsInt());
            priority = jObj.get("priority").getAsInt();
        }
        //TODO: picture
    }

    public static ArrayList buildUpdateQuery(JsonObject jObj, String path) throws IllegalArgumentException{
        ArrayList updateDoc = new ArrayList();

        if(jObj.has("name")){
            if(!Validate.name(jObj.get("name").getAsString())) throw new IllegalArgumentException("Unable to update. Invalid name min 2 chars.");
            addSetRequest(updateDoc,path + "name", jObj.get("name").getAsString());
        }

        if(jObj.has("fragile")){
            addSetRequest(updateDoc,path + "fragile", jObj.get("fragile").getAsBoolean());
        }

        if(jObj.has("destRoom")){
            addSetRequest(updateDoc,path + "destRoom", jObj.get("destRoom").getAsString());
        }

        if(jObj.has("packed")){
            addSetRequest(updateDoc,path + "packed", jObj.get("packed").getAsBoolean());
        }

        if(jObj.has("priority")){
            addSetRequest(updateDoc,path + "priority", jObj.get("priority").getAsInt());
        }

        if(jObj.has("description")){
            addSetRequest(updateDoc,path + "description", jObj.get("description").getAsString());
        }
        return updateDoc;
    }

    private static void addSetRequest(ArrayList doc, String key, Object obj){
        doc.add(new Document("$set", new Document().append(key, obj)));
    }


    Item(Document mongoDoc){
        if(!mongoDoc.containsKey("name") ||
                !Validate.name(mongoDoc.getString("name"))){
            System.out.println("WARNING!!!!! Missing key \"name\" when parsing mongo doc. We likely have bad data in the database.");
        }
        name = mongoDoc.getString("name");

        if(mongoDoc.containsKey("itemId")){
            id =  mongoDoc.getObjectId("itemId").toString();
        }

        if(mongoDoc.containsKey("fragile")){
            fragile = mongoDoc.getBoolean("fragile");
        }

        if(mongoDoc.containsKey("destRoom")){
            destinationRoomId = mongoDoc.getString("destRoom");
        }

        if(mongoDoc.containsKey("packed")){
            packed = mongoDoc.getBoolean("packed");
        }

        priority = 0;//new Priority();
        if(mongoDoc.containsKey("priority")){
            //priority.setPriority(mongoDoc.getInteger("priority"));
            priority = mongoDoc.getInteger("priority");
        }

        if(mongoDoc.containsKey("description")){
            description = mongoDoc.getString("description");
        }
        //TODO: picture
    }

    @Override
    public ObjectId getID() {

        return new ObjectId(this.id);
    }

    public String getIdAsString(){
        return this.id;
    }

    @Override
    public void setID(ObjectId id) {
        this.id = id.toString();
    }

    @Override
    public void setID(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String name) {
        if(Validate.name(name)){
            this.name = name;
        }else{
            throw new IllegalArgumentException("Name required");
        }
    }

    @Override
    public void setFragility(boolean fragile) {
        this.fragile = fragile;
    }

    @Override
    public boolean getFragility(){
        return fragile;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDestinationRoomId(String id){
        //TODO: Should we validate this?? Would require database check.
        this.destinationRoomId = id;
    }

    public String getDestinationRoomId(){
        return this.destinationRoomId;
    }

    public void setPackedState(boolean state){
        this.packed = state;
    }

    public boolean getPackedState(){
        return this.packed;
    }

    public int getPriority(){
        return this.priority;
    }

    public void setPriority(int priority){
        this.priority = priority;
    }
}
