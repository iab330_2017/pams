package PackManager.util;

import org.apache.commons.validator.routines.EmailValidator;

public class Validate {

    public static boolean name(String name){
        if(name == null || name.isEmpty() || name.length() < 2){
            return false;
        }
        return true;
    };

    public static boolean email(String email){
        EmailValidator validator = EmailValidator.getInstance();
        return validator.isValid(email);
    }

    public static boolean password(String pass){
        if(pass == null || pass.isEmpty() || pass.length() < 8){
            return false;
        }
        return true;
    }
}
