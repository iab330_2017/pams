package PackManager.util;

import org.eclipse.jetty.http.HttpStatus;
import spark.Filter;
import spark.Request;
import spark.Response;

import static PackManager.Main.loginController;
import static spark.Spark.halt;

public class Filters {

    // If a user manually manipulates paths and forgets to add
    // a trailing slash, redirect the user to the correct path
    public static Filter addTrailingSlashes = (Request request, Response response) -> {
        if (!request.pathInfo().endsWith("/")) {
            response.redirect(request.pathInfo() + "/");
        }
    };

    // Enable GZIP for all responses
    public static Filter addGzipHeader = (Request request, Response response) -> {
        response.header("Content-Encoding", "gzip");
    };

    // Enable GZIP for all responses
    public static Filter addJsonHeader = (Request request, Response response) -> {
        response.type("application/json");
    };

    public static Filter checkAuthorized = (Request request, Response response) -> {
        System.out.println("Checking auth for path: " + request.pathInfo());
        loginController.ensureUserIsLoggedIn(request, response);
    };

    public static Filter clientAcceptsJson = (Request request, Response response) -> {
        String accept = request.headers("Accept");
//        for ( String header: request.headers()) {
//            System.out.println("Header: " + header);
//        }
        if(accept == null || (!accept.contains("application/json"))){
            System.out.println("Client not accepting application/json return 406.");
            halt(HttpStatus.NOT_ACCEPTABLE_406, "Client must support application/json mime type to consume this service.");
        };
    };

}
