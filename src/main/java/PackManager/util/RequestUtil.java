package PackManager.util;

import org.apache.commons.lang3.StringUtils;
import spark.Request;

import org.apache.commons.codec.binary.Base64;
import java.util.HashMap;
import java.util.Map;

public class RequestUtil {

    public static Map<String, String> getQueryCredentials(Request request) throws IllegalArgumentException{
        Map<String, String> authMap = new HashMap();
        String encodedHeader;

        encodedHeader = request.headers("Authorization");
        if(encodedHeader == null){
            throw new IllegalArgumentException("Authorization header missing.");
        }
        try {
            encodedHeader = StringUtils.substringAfter(encodedHeader, "Basic");
        }catch (NullPointerException e){
            throw new IllegalArgumentException("Authorization header malformed or missing.");
        }
        String header = new String(Base64.decodeBase64(encodedHeader));
        //System.out.println("Decoded auth header: " + header);
        authMap.put("username", header.split(":")[0]);
        authMap.put("password", header.split(":")[1]);
        return authMap;
        //return request.queryParams("username");
    }

    public static String getQueryPassword(Request request) {
        try{
            String header = request.headers("Authorization");
            return header.split(":")[1];
        }catch(NullPointerException e){
            return "";
        }
        //return request.queryParams("password");
    }

    public static String getQueryLoginRedirect(Request request) {
        return request.queryParams("loginRedirect");
    }

    public static String getSessionLocale(Request request) {
        return request.session().attribute("locale");
    }

    public static String getSessionCurrentUser(Request request) {
        return request.session().attribute("currentUser");
    }

    public static boolean removeSessionAttrLoggedOut(Request request) {
        Object loggedOut = request.session().attribute("loggedOut");
        request.session().removeAttribute("loggedOut");
        return loggedOut != null;
    }

    public static String removeSessionAttrLoginRedirect(Request request) {
        String loginRedirect = request.session().attribute("loginRedirect");
        request.session().removeAttribute("loginRedirect");
        return loginRedirect;
    }



}
