package PackManager.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.MalformedJsonException;
import spark.ResponseTransformer;

public class JsonUtil {
    public static String toJson(Object object) {
        //System.out.println(new Gson().toJson(object));
        return new Gson().toJson(object);
    }

    public static JsonObject toJsonObj(String json) throws JsonSyntaxException{
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
            return gson.fromJson(json, JsonObject.class);
    }

    public static ResponseTransformer json() {
        return JsonUtil::toJson;
    }
}
