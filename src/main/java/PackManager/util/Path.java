package PackManager.util;

public class Path {
        public static final String LOGIN = "/login/";
        public static final String LOGOUT = "/logout/";

        public static final String USER = "/user/:id/";
        public static final String NEW_USER = "/user/";

        public static final String MOVES = "/api/move/";
        public static final String MOVE  = "/api/move/:id";

        // public static final String BOXES = "/api/box/";
        public static final String BOX_BY_MOVE   = "/api/move/:moveId/box/:boxId/";
        public static final String BOXES_BY_MOVE = "/api/move/:moveId/box/";

        public static final String ITEMS_IN_BOX = "/api/move/:moveId/box/:boxId/item/";
        public static final String ITEM_IN_BOX  = "/api/move/:moveId/box/:boxId/item/:itemid/";

        public static final String ITEMS = "/api/item/";
        public static final String ITEM = "/api/item/:id";
        public static final String ITEM_SEARCH = "/api/item/search";

        //public static final String LOCATIONS = "/api/move/:moveId/location/";
        //public static final String LOCATION  = "/api/move/:movedId/location/:locationId/";

        public static final String ROOMS_IN_MOVE = "/api/move/:moveId/room/";
        public static final String ROOM_IN_MOVE  = "/api/move/:moveId/room/:Id/";

}