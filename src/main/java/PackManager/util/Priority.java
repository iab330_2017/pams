package PackManager.util;

public class Priority {
    private int priority;
    private boolean notSet;

    public Priority(){
        this.priority = 0;
        this.notSet = true;
    }

    public int setPriority(int priority){
        this.notSet = false;
        if(priority > 10){
            this.priority = 10;
            return this.priority;
        }else if(priority < 0){
            priority = 0;
            return this.priority;
        }else{
            this.priority = priority;
        }
        return priority;
    };

    public int increasePriority(){
        this.notSet = false;
        if(this.priority >= 10){
            return this.priority;
        }else{
            this.priority ++;
            return this.priority;
        }
    }

    public int decreasePriority(){
        this.notSet = false;
        if(this.priority <= 0){
            return this.priority;
        }else{
            this.priority --;
            return this.priority;
        }
    }

    public int notSet(){
        this.notSet = false;
        this.priority = 0;
        return this.priority;
    };

    public int get(){
        return this.priority;
    }
}
