package PackManager.util;

import org.eclipse.jetty.http.HttpStatus;
import spark.Response;

import java.util.HashMap;
import java.util.Map;

public final class errorResponse {
    public static Map<String, String> malformedJson(Exception e, Response response){
        response.status(HttpStatus.BAD_REQUEST_400);
        return buildMap("400_BAD_REQUEST", "MalformedJsonException: " + e.getCause());
    }

    public static Map<String, String> illegalArgument(Exception e, Response response){
        response.status(HttpStatus.BAD_REQUEST_400);
        return buildMap("400_BAD_REQUEST", "IllegalArgumentException: " + e.getMessage());
    }

    public static Map<String, String> clientNoJson(Response response){
        System.out.println("No JSON returning 406.");
        response.status(HttpStatus.NOT_ACCEPTABLE_406);
        return buildMap("406_NOT_ACCEPTABLE", "Client must support application/json mime type to consume this service.");
    }

    public static Map<String, String> invalidUserID(Response response){
        response.status(HttpStatus.BAD_REQUEST_400);
        return buildMap("400_BAD_REQUEST", "Missing or invalid user id");
    }

    public static Map<String, String> itemNotFoundInDatabase(Response response, String Message){
        response.status(HttpStatus.NOT_FOUND_404);
        return buildMap("404_NOT_FOUND", Message);
    }

    public static Map<String, String> unauthorizedAccess(Response response){
        response.status(HttpStatus.FORBIDDEN_403);
        return buildMap("403_FORBIDDEN", "You do not have permission to access this resource");
    }

    private static Map<String, String> buildMap(String errorCode, String description){
        Map<String, String> responseMap = new HashMap<String, String>();
        responseMap.put("error", errorCode);
        responseMap.put("errorDescription", description);
        return responseMap;
    }
}
