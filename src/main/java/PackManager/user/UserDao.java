package PackManager.user;

import com.mongodb.BasicDBList;
import com.mongodb.MongoWriteException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.types.ObjectId;
import spark.Request;

import java.util.ArrayList;

import static PackManager.login.LoginController.getSessionUserID;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Updates.pull;

public class UserDao {
    private static MongoCollection<Document> usersCollection;

    public UserDao(final MongoDatabase packManDB) {
        usersCollection = packManDB.getCollection("users");
    }

    public static User getUserByID(String id){
        System.out.println("Getting user: " + id);
        Document userDoc;
        userDoc = usersCollection.find(eq("_id", new ObjectId(id))).first();
        if(userDoc == null){
            System.out.println("User not found. Return null");
            return null;
        }

        return buildUserFromDoc(userDoc);
    }

    public static User getUserByEmail(String email){
        System.out.println("Getting user with email: " + email);
        Document userDoc;
        userDoc = usersCollection.find(eq("email", email)).first();
        if(userDoc == null){
            System.out.println("User not found. Return null");
            return null;
        }

        return buildUserFromDoc(userDoc);
    }

    public boolean updateUser(User user) throws MongoWriteException{
        System.out.println("Updating user: " + user.getId().toString());
        Document userDoc = new Document()
                .append("firstName", user.getFirstName())
                .append("lastName",  user.getLastName())
                .append("email",     user.getEmail())
                .append("hashedPassword", user.getHashedPassword())
                .append("salt",      user.getSalt());

        usersCollection.findOneAndUpdate(eq("_id", user.getId()), new Document("$set", userDoc));
        return true;
    }

    public ObjectId createUser(User user) throws MongoWriteException{
        //TODO: Test for valid email and load into database by calling update
        //NOTE: This user should NOT have a objectID. If it does it will update instead of replacing
        Document userDoc = new Document()
                .append("firstName", user.getFirstName())
                .append("lastName",  user.getLastName())
                .append("email",     user.getEmail())
                .append("hashedPassword", user.getHashedPassword())
                .append("salt",      user.getSalt());

            usersCollection.insertOne(userDoc);

        return userDoc.getObjectId("_id");
    }

    //TODO: Should this be here? It's definitely a DAO thing but it might need to be in the controller.
    public void addMoveToUser(String userID, ObjectId moveID) throws MongoWriteException{
        //System.out.println("Adding move to user: " + userID);
        usersCollection.updateOne(eq("_id", new ObjectId(userID)), Updates.addToSet("moves", moveID));
    }

    public void removeMoveFromUser(String userID, ObjectId moveID){
        usersCollection.updateOne(eq("_id", new ObjectId(userID)), pull("moves", moveID) );
    }

    public ArrayList getMovesForUser(String userID){
        ArrayList moves;
        Document userDoc;
        userDoc = usersCollection.find(eq("_id", new ObjectId(userID)))
                .projection(fields(include("moves"), excludeId()))
                .first();
        if(userDoc == null || !userDoc.containsKey("moves"))return new ArrayList();
       return (ArrayList) userDoc.get("moves");
    }

    private static User buildUserFromDoc(Document userDoc){
        User user = new User(userDoc.getObjectId("_id"));
        user.setEmail(userDoc.getString("email"));
        user.setFirstName(userDoc.getString("firstName"));
        user.setLastName(userDoc.getString("lastName"));
        user.setHashedPassword(userDoc.getString("hashedPassword"));
        user.setSalt(userDoc.getString("salt"));
        return user;
    }
}
