package PackManager.user;

import com.google.gson.JsonSyntaxException;
import com.mongodb.MongoWriteException;
import org.eclipse.jetty.http.HttpStatus;
import org.mindrot.jbcrypt.BCrypt;
import spark.*;

import java.util.HashMap;
import java.util.Map;

import static PackManager.util.JsonUtil.toJsonObj;
import static PackManager.Main.userDao;
import static PackManager.util.errorResponse.*;
import static PackManager.Main.loginController;

public class UserController {
    public static Route getUserDetails = (Request request, Response response) -> {
        loginController.ensureUserIsLoggedIn(request, response);
        Map<String, String> responseMap = new HashMap<String, String>();

        User user;
        if(!loginController.getSessionUserID(request).equals(request.params("id"))){
            return unauthorizedAccess(response);
        }
        try {
            user = userDao.getUserByID(request.params("id"));
        }catch(IllegalArgumentException e){
            return invalidUserID(response);
        }
        if(user == null){
            return itemNotFoundInDatabase(response, "User was not found in the database");
        }
        response.status(HttpStatus.OK_200);
        return user;
    };


    public static Route addNewUser = (Request request, Response response) ->{
        Map<String, String> responseMap = new HashMap<String, String>();

        User newUser;
        try{
            newUser = new User(toJsonObj(request.body()));
        }catch(IllegalArgumentException e){
            return illegalArgument(e, response);
        }catch(JsonSyntaxException e){
            System.out.println("Caught malformed json exception");
            return malformedJson(e, response);
        }
        String newID = "";
        try{
            newID = userDao.createUser(newUser).toString();
        }catch(MongoWriteException e){
            response.status(HttpStatus.BAD_REQUEST_400);
            return e.getError();
        }
        response.status( HttpStatus.CREATED_201);
        responseMap.put("id", newID);
        return responseMap;
    };

    public static Route updateUser = (Request request, Response response) -> {
        loginController.ensureUserIsLoggedIn(request, response);
        Map<String, String> responseMap = new HashMap<String, String>();
        User user;
        if(!loginController.getSessionUserID(request).equals(request.params("id"))){
            return unauthorizedAccess(response);
        }

        try {
            user = userDao.getUserByID(request.params("id"));
        }catch(IllegalArgumentException e){
            return invalidUserID(response);
        }

        try{
            user.updateFromJson(toJsonObj(request.body()));
        }catch(IllegalArgumentException e){
            return illegalArgument(e, response);
        }catch(JsonSyntaxException e){
            return malformedJson(e, response);
        }
        boolean success = false;
        try{
            success = userDao.updateUser(user);
        }catch(MongoWriteException e){
            response.status(HttpStatus.BAD_REQUEST_400);
            return e.getError();
        }
        response.status(HttpStatus.CREATED_201);
        return success;
    };

    public void deleteUser(){/*Not Yet Implemented*/}

    public static boolean authenticate(String email, String password){
        if (    email == null || password == null ||
                email.isEmpty() || password.isEmpty()) {
            System.out.println("Authenticating user email/password missing.");
            return false;
        }

        User user = UserDao.getUserByEmail(email);
        if (user == null) {
           return false;
        }

        String hashedPassword = BCrypt.hashpw(password, user.getSalt());
        return hashedPassword.equals(user.getHashedPassword());
    }

}
