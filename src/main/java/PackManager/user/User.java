package PackManager.user;

import PackManager.util.Validate;
import com.google.gson.*;
import org.apache.commons.validator.routines.EmailValidator;
import org.bson.types.ObjectId;
import org.mindrot.jbcrypt.BCrypt;

import static PackManager.util.JsonUtil.toJsonObj;

public class User {

    private String email;
    private String firstName;
    private String lastName;
    private transient String hashedPassword;
    private transient String salt;
    private String id;

    public User(){};

    public User(ObjectId objectId){id = objectId.toString();};

    public User(ObjectId objectId, String email, String lastName, String firstName, String password){
        if(email == null || objectId == null || email.isEmpty()){
            throw new IllegalArgumentException("Email address and objectId are required");
        }
        this.id = objectId.toString();
        this.email = email;
        this.lastName = lastName;
        this.firstName = firstName;
        this.setPasswordAndHash(password);
    }

    public User(String email, String lastName, String firstName, String password){
        if(email == null || email.isEmpty()){
            throw new IllegalArgumentException("Email address is required");
        }
        this.email = email;
        this.lastName = lastName;
        this.firstName = firstName;
        this.setPasswordAndHash(password);
    }

    public User(JsonObject jObj) throws JsonSyntaxException{
         /* TODO: We may be able to refactor here in order to tidy up a bit.
          * Maybe catch the missing keys in try/catch block instead of testing
          * for them all explicitly. */
        if(   !(jObj.has("email") &&
                jObj.has("firstName") &&
                jObj.has("lastName")  &&
                jObj.has("password"))){
            throw new IllegalArgumentException("Missing JSON Parameter. email, firstName, lastName and password are required");
        }

            email = jObj.get("email").getAsString();
            firstName = jObj.get("firstName").getAsString();
            lastName = jObj.get("lastName").getAsString();
            String password = jObj.get("password").getAsString();
        if(     !Validate.email(email) ||
                !Validate.name(firstName) ||
                !Validate.name(lastName) ||
                !Validate.password(password)){
            throw new IllegalArgumentException("Invalid JSON Parameter Email address, password (8 chars+), first and last name are required");
        }
        setPasswordAndHash(password);

    }

    public void updateFromJson(JsonObject jObj) throws IllegalArgumentException{
        Boolean validParams = false;
        if(jObj.has("firstName")){
            this.setFirstName(jObj.get("firstName").getAsString());
            validParams = true;
        }

        if(jObj.has("lastName")){
            this.setLastName(jObj.get("lastName").getAsString());
            validParams = true;
        }

        if(jObj.has("email")){
            this.setEmail(jObj.get("email").getAsString());
            validParams = true;
        }

        if(jObj.has("password")){
            this.setPasswordAndHash(jObj.get("password").getAsString());
            validParams = true;
        }

        if(!validParams){
            throw new IllegalArgumentException("No valid update paramaters");
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if(!Validate.email(email)){
            throw new IllegalArgumentException("Invalid Email");
        }
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if(!Validate.name(firstName)){
            throw new IllegalArgumentException("Invalid first Name");
        }
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if(!Validate.name(lastName)){
            throw new IllegalArgumentException("Invalid last Name");
        }
        this.lastName = lastName;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getSalt() {
        return salt;
    }

    public void setPasswordAndHash(String newPassword) throws IllegalArgumentException{
        if(!Validate.password(newPassword)){
            throw new IllegalArgumentException("Password invalid. Minimum 8 chars");
        }

        setSalt(BCrypt.gensalt());
        hashedPassword = (BCrypt.hashpw(newPassword, getSalt()));
    }

    public void setHashedPassword(String hashedPassword){
        this.hashedPassword = hashedPassword;
    }

    public ObjectId getId() {
        return new ObjectId(id);
    }




}
